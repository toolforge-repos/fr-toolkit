import fs from "fs";
import path from "path";
import { fileURLToPath } from "url";
import { domain, storagePath } from "../../toolkit_config.js";

const jsonBuilder = {
  projects: [
    ["campaigns", "annual_fund"],
    ["campaigns", "endowment"],
    ["campaigns", "miscellaneous"],
  ],
  build: function () {
    try {
      this.projects.forEach(([project, goal]) => {
        console.log(`Building JSON for project "${project}" | goal "${goal}"...`);
        // Creating rootPath
        const __filename = fileURLToPath(import.meta.url);
        const __dirname = path.dirname(__filename);
        const rootPath = path.resolve(__dirname, "../../");

        // Where to read folders/files from.
        const targetFolderPath = `${rootPath}/${storagePath}/fr-email-templates/templates-shared/${project}/${goal}`;

        // Where the JSON will be created.
        const outputFilePath = `${rootPath}/${storagePath}/data/campaigns/${goal}.json`;

        // Regular expressions to match email metadata in .html files.
        // Don't use '>' in any of these values.
        const templateRegex = /<template(?:[^>]|\s+(?!id=)[^>])*\sid="email-metadata"(?:[^>]|\s+(?!id=)[^>])*>\s*<\/template>/;
        const nameRegex = /data-name="([^"]*)"/;
        const fromRegex = /data-from="([^"]*)"/;
        const subjectLineRegex = /data-subject-line="([^"]*)"/;
        const preheaderRegex = /data-preheader="([^"]*)"/;

        const keyMap = [
          { key: "name", category: "countries" },
          { key: "country", category: "languages" },
          { key: "language", category: "campaigns" },
          { key: "campaign", category: "files" },
        ];

        function getFolderStructure(dirPath, depth = 0) {
          const result = [];
          const items = fs.readdirSync(dirPath, { withFileTypes: true });

          // Custom natural sort function
          items.sort((a, b) => {
            const splitAlphaNumeric = (name) => {
              return name.split(/(\d+)/).map((part) => (!isNaN(part) && part !== "" ? parseInt(part, 10) : part));
            };

            const partsA = splitAlphaNumeric(a.name);
            const partsB = splitAlphaNumeric(b.name);

            for (let i = 0; i < Math.max(partsA.length, partsB.length); i++) {
              const partA = partsA[i] !== undefined ? partsA[i] : "";
              const partB = partsB[i] !== undefined ? partsB[i] : "";

              if (partA !== partB) {
                if (typeof partA === "number" && typeof partB === "number") {
                  return partA - partB;
                }
                return String(partA).localeCompare(String(partB));
              }
            }
            return 0;
          });

          items.forEach((item) => {
            if (item.isDirectory()) {
              const subStructure = getFolderStructure(path.join(dirPath, item.name), depth + 1);
              const currentLevel = keyMap[depth];

              if (currentLevel) {
                result.push({
                  [currentLevel.key]: item.name,
                  [currentLevel.category]: subStructure,
                });
              } else {
                result.push({
                  name: item.name,
                  children: subStructure,
                });
              }
            } else if (path.extname(item.name) === ".html") {
              const filePath = path.join(dirPath, item.name);

              let urlPath = filePath.slice(filePath.indexOf("/storage"));
              if (urlPath.includes("#")) urlPath = urlPath.replace(/#/g, "%23");

              const htmlString = fs.readFileSync(filePath, "utf8");
              const emailMetadata = getMetadata(htmlString);
              result.push({
                name: emailMetadata.name,
                url: `${domain}${urlPath}`,
                from: emailMetadata.from,
                subject_line: emailMetadata.subjectLine,
                preheader: emailMetadata.preheader,
                acoustic_template: item.name.split(".html")[0],
              });
            }
          });

          return result;
        }

        function getMetadata(htmlString) {
          const metadata = htmlString.match(templateRegex);
          if (metadata) {
            const name = metadata[0].match(nameRegex)[1];
            const from = metadata[0].match(fromRegex)[1];
            const subjectLine = metadata[0].match(subjectLineRegex)[1];
            const preheader = metadata[0].match(preheaderRegex)[1];

            return {
              name: name || null,
              from: from || null,
              subjectLine: subjectLine || null,
              preheader: preheader || null,
            };
          } else {
            return {
              name: "No 'name' value present in file",
              from: "No 'from' value present in file",
              subjectLine: "No 'subject-line' value present in file",
              preheader: "No 'preheader' value present in file",
            };
          }
        }

        const folderStructure = getFolderStructure(targetFolderPath);

        const finalStructure = {
          fiscal_year: folderStructure,
          last_updated: new Date().toUTCString(),
        };

        const outputDir = path.dirname(outputFilePath);
        fs.mkdirSync(outputDir, { recursive: true });
        fs.writeFileSync(outputFilePath, JSON.stringify(finalStructure, null, 2));

        console.log(`JSON created at ${outputFilePath}`);
        console.log(`JSON built successfully for project "${project}" | goal "${goal}".`);
      });
    } catch (error) {
      console.error("Error from json-builder.js:", error);
    }
  },
};

export default jsonBuilder;

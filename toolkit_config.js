/* Defining the domain for file referencing and routing. When built for production, it is updated to reflect the actual server domain. */
export const domain = process.env.NODE_ENV === "development" ? "http://localhost:5173" : "https://fr-toolkit.toolforge.org";
export const storagePath = process.env.NODE_ENV === "development" ? "storage_dev" : "storage";

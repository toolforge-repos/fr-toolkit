import { createRouter, createWebHashHistory } from "vue-router";
import EmailArchive from "@/src/routes/email/EmailArchive.vue";
import EmailCampaigns from "@/src/routes/email/EmailCampaigns.vue";
import EmailDiffChecker from "@/src/routes/email/EmailDiffChecker.vue";
import EmailComparator from "@/src/routes/email/EmailComparator.vue";
import NotFound from "@/src/routes/misc/NotFound.vue";

// To trick Vue Router for /abba route.
const EmptyComponent = { template: "<div></div>" };

const routes = [
  { path: "/", component: EmailDiffChecker, meta: { title: "Email | Diffchecker" } },
  {
    path: "/abba",
    component: EmptyComponent,
    beforeEnter() {
      window.location.replace("/abba/abba.html");
    },
  },
  { path: "/email/archive", component: EmailArchive, meta: { title: "Email | Archive" } },
  {
    path: "/email/campaigns/:goal",
    component: EmailCampaigns,
    meta: { title: "Email | Campaigns" },
    beforeEnter: (to, from, next) => {
      const validCampaigns = ["annual_fund", "endowment", "miscellaneous"];
      if (validCampaigns.includes(to.params.goal)) next();
      else next({ name: "NotFound" });
    },
  },
  { path: "/email/diffchecker", component: EmailDiffChecker, meta: { title: "Email | Diffchecker" } },
  { path: "/email/comparator", component: EmailComparator, meta: { title: "Email | Comparator" } },
  { path: "/:pathMatch(.*)*", component: NotFound, meta: { title: "404 Not Found" } },
  { path: "/404", name: "NotFound", component: NotFound, meta: { title: "404 Not Found" } },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || "FR-Toolkit";
  next();
});

export default router;

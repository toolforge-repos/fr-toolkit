const ROUTES = [
  {
    category: "General",
    links: [{ title: "ABBA Testing Statistics", to: "/abba" }],
  },
  {
    category: "Email",
    links: [
      { title: "Campaigns - Annual Fund", to: "/email/campaigns/annual_fund" },
      { title: "Campaigns - Endowment", to: "/email/campaigns/endowment" },
      { title: "Campaigns - Miscellaneous", to: "/email/campaigns/miscellaneous" },
      { title: "Diffchecker", to: "/email/diffchecker" },
      { title: "Comparator", to: "/email/comparator" },
      { title: "Archive", to: "/email/archive" },
    ],
  },
];

export { ROUTES };

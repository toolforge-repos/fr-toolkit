<script setup>
// ========================================
// Imports
// ========================================
import { domain, storagePath } from "@/toolkit_config.js";
import { ROUTES } from "@/src/routes.js";
import { ref, onMounted, watch, computed } from "vue";
import { useRoute, useRouter } from "vue-router";
import html2canvas from "html2canvas";
import { CdxField, CdxTextInput, CdxSelect, CdxAccordion, CdxButton, CdxIcon, CdxDialog, CdxToggleSwitch } from "@wikimedia/codex";
import { cdxIconLink, cdxIconImage, cdxIconAlert } from "@wikimedia/codex-icons";
import "@wikimedia/codex-design-tokens/theme-wikimedia-ui.css";

// ========================================
// Variables
// ========================================

/**
 * Routing and data variables.
 * @var jsonURL points to the .json file created during the import from Google Drive API.
 * @var emailData is populated with the response from 'jsonURL'
 */
const validRoutes = ROUTES.flatMap((category) => category.links.map((link) => link.to));
const route = useRoute();
const router = useRouter();
const goal = ref(route.params.goal);
const jsonURL = `${domain}/${storagePath}/data/campaigns/${goal.value}.json`;
const emailData = ref(null);

/**
 * Arrays populated with the JSON data for each level.
 */
const fiscalYears = ref([]);
const countries = ref([]);
const languages = ref([]);
const categories = ref([]);

/**
 * User choices when they select an email.
 */
const selectedFiscalYear = ref(null);
const selectedCountry = ref(null);
const selectedLanguage = ref(null);
const selectedEmail = ref({
  url: null,
  src: null,
  from: null,
  subject_line: null,
  preheader: null,
});

const userFilter = ref({ input: "", disabled: true });

/**
 * User interface state variables
 */
const displayMetadata = ref(true);
const clipboardMessage = ref({ display: false, message: "" });
const jsonMissing = ref(null);
const jsonLastUpdated = ref(null);
const fetchingJsonData = ref(false);

// ========================================
// Functions
// ========================================

/**
 * Runs when the page is first loaded.
 * Checks to see if the .json file is present on fr-toolkit.toolforge.org. If the file is missing, the user is prompted to run an import.
 */
onMounted(async () => {
  const response = await fetch(jsonURL, { method: "HEAD" });
  if (response.status === 200) {
    await fetchEmailData();
  } else {
    jsonMissing.value = true;
  }
});

/**
 * Allows the 'EmailCampaigns' SFC to be loaded only for "campaigns/annual_fund"  and "campaigns/endowment".
 * This may be updated in the future for more dynamic use.
 */
watch(
  () => route.fullPath,
  (newPath, oldPath) => {
    if (!validRoutes.includes(route.fullPath)) {
      router.push("/404");
    } else {
      window.location.href = `#${newPath}`; // Reload the page to the new valid route
      window.location.reload(true);
    }
  },
);

/**
 * Fetches the .json file and populates the 'emailData' variable.
 */
const fetchEmailData = async () => {
  try {
    const response = await fetch(jsonURL);
    if (!response.ok) throw new Error("JSON retrieval was not successful. The file may not exist.");
    emailData.value = await response.json();
    if (emailData.value) {
      jsonLastUpdated.value = emailData.value.last_updated;
      fiscalYears.value = emailData.value.fiscal_year.map((year) => ({ label: String(year.name).toUpperCase(), value: year.name })); // Mapping fiscal years
    }
  } catch (error) {
    console.error("Error fetching data:", error);
  }
};

/**
 * Updates "selectedEmail" with the currently chosen email by the user, which is shown in <!-- Design Preview --> section.
 * @param email - Object holding a single email's data.
 */
const updateSelectedEmail = (email) => {
  selectedEmail.value = { ...email };
};

/**
 * Resets the dropdown choices for a specific level and its children.
 * @param level - Describes from which level (inclusive and children) should the selection be reset
 * @example If you run resetChoices("all"), all dropdown options will be cleared.
 * @example If you run resetChoices("fiscal_year"), the fiscal year selection will remain intact. However the remaining dropdown options will be reset.
 */
const resetChoices = (level) => {
  switch (level) {
    case "all":
      fiscalYears.value = [];
      countries.value = [];
      languages.value = [];
      categories.value = [];
      selectedFiscalYear.value = null;
      selectedCountry.value = null;
      selectedLanguage.value = null;
      break;
    case "fiscal_year":
      countries.value = [];
      languages.value = [];
      categories.value = [];
      selectedCountry.value = null;
      selectedLanguage.value = null;
      break;
    case "country":
      languages.value = [];
      categories.value = [];
      selectedLanguage.value = null;
      break;
    case "language":
      categories.value = [];
      break;
  }
  Object.keys(selectedEmail.value).forEach((key) => (selectedEmail.value[key] = null));
};

// Note to dev: the following three watcher functions could possibly be simplified.
/**
 * Watcher function. When a fiscal year is chosen by the user, the countries dropdown is populated accordingly.
 */
watch(selectedFiscalYear, (newValue) => {
  resetChoices("fiscal_year");
  const chosenFiscalYear = emailData.value.fiscal_year.find((year) => year.name === newValue); // Finding selected fiscal year
  if (chosenFiscalYear) {
    countries.value = chosenFiscalYear.countries.map((country) => {
      const capitalizedWords = country.country
        .split("_")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
      return { label: capitalizedWords, value: country.country };
    });
    Object.assign(userFilter.value, { input: "", disabled: true });
  }
});

/**
 * Watcher function. When a country is chosen by the user, the languages dropdown is populated accordingly.
 */
watch(selectedCountry, (newValue) => {
  resetChoices("country");
  const chosenFiscalYear = emailData.value.fiscal_year.find((year) => year.name === selectedFiscalYear.value); // Find the currently selected fiscal year
  if (chosenFiscalYear) {
    const chosenCountry = chosenFiscalYear.countries.find((country) => country.country === newValue); // Find the selected country within the selected fiscal year
    if (chosenCountry) {
      languages.value = chosenCountry.languages.map((language) => ({
        label: language.language.charAt(0).toUpperCase() + language.language.slice(1),
        value: language.language,
      }));
      Object.assign(userFilter.value, { input: "", disabled: true });
    }
  }
});

/**
 * Watcher function. When a language is chosen by the user, the left sidebar is populated with campaigns accordingly.
 */
watch(selectedLanguage, (newValue) => {
  resetChoices("language");
  const chosenFiscalYear = emailData.value.fiscal_year.find((year) => year.name === selectedFiscalYear.value);
  if (chosenFiscalYear) {
    const chosenCountry = chosenFiscalYear.countries.find((country) => country.country === selectedCountry.value);
    if (chosenCountry) {
      const chosenLanguage = chosenCountry.languages.find((language) => language.language === newValue);
      if (chosenLanguage) {
        categories.value = chosenLanguage.campaigns;
        categories.value.forEach((element) => {
          element.campaign = String(element.campaign)
            .replace(/_/g, " ")
            .replace(/rml/gi, "RML")
            .replace(/email/gi, "Email")
            .replace(/cl/gi, "CL")
            .split(" ")
            .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
            .join(" ");
        });

        // Enable user filtering on campaigns.
        userFilter.value.disabled = false;
      }
    }
  }
});

// Computed property to filter categories based on search term
const filteredCategories = computed(() => {
  if (!userFilter.value.input) return categories.value; // If no search input, return all categories.

  return categories.value.filter((category) => {
    const searchTerm = userFilter.value.input.toLowerCase();

    // Check if the category name matches
    const categoryNameMatches = category.campaign.toLowerCase().includes(searchTerm);

    // Check if any file name matches
    const emailNameMatches = category.files.some((email) => email.name.toLowerCase().includes(searchTerm));

    // Include the category if either its name or one of its files matches
    return categoryNameMatches || emailNameMatches;
  });
});

// Helper to check if a file name matches the search term
const isFileHighlighted = (emailName) => {
  if (userFilter.value.input) {
    const searchTerm = userFilter.value.input.toLowerCase();
    return emailName.toLowerCase().includes(searchTerm);
  }
};

/**
 * Creates a URL pointing to the email file directly and copies it to the user's clipboard.
 */
const copyEmailURL = () => {
  navigator.clipboard.writeText(selectedEmail.value.url).then(() => {
    // Show URL copied message
    clipboardMessage.value.display = true;
    clipboardMessage.value.message = "Email URL copied!";
    setTimeout(() => {
      clipboardMessage.value.display = false;
    }, 3000);
  });
};

/**
 * Creates a screenshot of the current email preview and copies it to the user's clipboard.
 */
const createScreenshot = () => {
  const options = {
    allowTaint: true,
    useCORS: true,
  };
  const emailIframe = document.getElementById("design-preview").contentWindow.document.body;
  html2canvas(emailIframe, options).then((canvas) => canvas.toBlob((blob) => navigator.clipboard.write([new ClipboardItem({ "image/png": blob })])));
  clipboardMessage.value.display = true;
  clipboardMessage.value.message = "Screenshot copied!";
  setTimeout(() => {
    clipboardMessage.value.display = false;
  }, 3000);
};
</script>

<template>
  <section class="overlay relative min-h-screen" :class="{ 'overlay-enabled': fetchingJsonData }">
    <cdx-dialog class="dialog" v-model:open="jsonMissing" title="JSON missing from FR-Toolkit.">
      <template #header>
        <div>
          <h2 class="text-lg font-bold"><cdx-icon :icon="cdxIconAlert" class="mr-2" />JSON missing from FR-Toolkit.</h2>
        </div>
      </template>
      <p>Contact <a class="text-blue-600 underline" href="mailto:ppenloglou@wikimedia.org">ppenloglou@wikimedia.org</a></p>
    </cdx-dialog>
    <div class="flex flex-row items-center justify-between border-b border-gray-300 bg-gray-100 p-4">
      <div class="flex flex-row gap-4">
        <cdx-field class="m-0">
          <cdx-select :disabled="fiscalYears.length === 0" v-model:selected="selectedFiscalYear" :menu-items="fiscalYears" default-label="Choose fiscal year" />
          <template #label> Fiscal Year </template>
        </cdx-field>
        <cdx-field class="m-0">
          <cdx-select :disabled="countries.length === 0" v-model:selected="selectedCountry" :menu-items="countries" default-label="Choose country" />
          <template #label> Country </template>
        </cdx-field>
        <cdx-field class="m-0">
          <cdx-select :disabled="languages.length === 0" v-model:selected="selectedLanguage" :menu-items="languages" default-label="Choose language" />
          <template #label> Language </template>
        </cdx-field>
      </div>
      <div class="flex flex-col gap-2">
        <span class="text-right text-sm text-gray-500"><strong>GitLab sync</strong>: {{ jsonLastUpdated }}</span>
        <div class="flex justify-end gap-2">
          <cdx-toggle-switch v-model="displayMetadata">Display Metadata</cdx-toggle-switch>
        </div>
      </div>
    </div>
    <section v-if="emailData !== null" class="flex">
      <!-- Design Selector -->
      <div>
        <div class="flex max-h-screen min-w-80 max-w-80 flex-col overflow-auto">
          <!-- User Filter Field -->
          <div class="p-4">
            <cdx-text-input v-model="userFilter.input" :disabled="userFilter.disabled" placeholder="Filter results..." />
          </div>
          <cdx-accordion v-for="category in filteredCategories">
            <template #title>
              <span class="font-bold">{{ category.campaign }}</span>
            </template>
            <ul class="flex flex-col gap-1">
              <li
                class="cursor-pointer p-2 transition duration-300 hover:bg-gray-200"
                :class="{
                  'bg-blue-100': selectedEmail.url === email.url,
                  'text-blue-600': selectedEmail.url === email.url,
                  'bg-yellow-100': isFileHighlighted(email.name),
                }"
                v-for="(email, index) in category.files"
                :key="email.acoustic_template"
                @click="updateSelectedEmail(email)">
                <span class="text-sm font-bold">{{ index + 1 }}.</span>
                {{ email.name }}
              </li>
            </ul>
          </cdx-accordion>
        </div>
      </div>
      <!-- Design Preview -->
      <div class="flex flex-1 flex-col border-b border-l border-gray-300 bg-gray-100">
        <div v-show="displayMetadata" class="flex flex-row items-start justify-between border-b border-gray-300 p-4">
          <!-- Table for email info -->
          <table>
            <tr>
              <td class="pb-2 pr-5 text-sm font-light text-gray-700">From:</td>
              <td class="pb-2">{{ selectedEmail.from }}</td>
            </tr>
            <tr>
              <td class="pb-2 pr-5 text-sm font-light text-gray-700">Subject Line:</td>
              <td class="pb-2 font-semibold">{{ selectedEmail.subject_line }}</td>
            </tr>
            <tr>
              <td class="pb-4 pr-5 text-sm font-light text-gray-700">Preheader:</td>
              <td class="pb-4">{{ selectedEmail.preheader }}</td>
            </tr>
            <tr>
              <td class="pr-5 text-sm font-light text-gray-700">Acoustic Name:</td>
              <td class="text-sm font-light text-gray-700">{{ selectedEmail.acoustic_template }}</td>
            </tr>
          </table>
          <div class="relative flex flex-col gap-2">
            <transition name="fade" mode="out-in">
              <div v-show="clipboardMessage.display" class="absolute -bottom-14 right-0 rounded-sm border border-green-700 bg-green-100 p-2">
                {{ clipboardMessage.message }}
              </div>
            </transition>
            <cdx-button :disabled="selectedEmail.url === null" @click="copyEmailURL"><cdx-icon :icon="cdxIconLink" /> Copy Email URL</cdx-button>
            <cdx-button :disabled="selectedEmail.url === null" @click="createScreenshot"><cdx-icon :icon="cdxIconImage" /> Create Screenshot</cdx-button>
          </div>
        </div>
        <iframe sandbox="allow-same-origin allow-popups allow-scripts" id="design-preview" class="h-[calc(100vh-150px)]" :src="selectedEmail.url"></iframe>
      </div>
    </section>
  </section>
</template>

<style>
.overlay::before {
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: black;
  transition: opacity 0.3s;
  z-index: 10;
  opacity: 0;
  pointer-events: none;
}

.overlay-enabled::before {
  opacity: 0.2;
  transition: opacity 0.3s;
  pointer-events: auto;
}

.overlay-enabled::after {
  content: "";
  position: absolute;
  top: 50%;
  left: 50%;
  width: 40px;
  height: 40px;
  margin-top: -20px; /* Half of the height */
  margin-left: -20px; /* Half of the width */
  border: 3px solid #36c;
  border-radius: 50%;
  border-top-color: transparent;
  animation: spin 1s linear infinite; /* Animation for spinning */
}

.dialog header {
  background: #fc3;
}
@keyframes spin {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}
</style>

/** @type {import('tailwindcss').Config} */
// Theme extension based on Wikimedia Codex Design System
const colors = require('tailwindcss/colors')
export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: [
          "-apple-system",
          "BlinkMacSystemFont",
          "Segoe UI",
          "Roboto",
          "Lato",
          "Helvetica",
          "Arial",
          "sans-serif",
        ],
        serif: [
          "Linux Libertine",
          "Georgia",
          "Times",
          "Source Serif Pro",
          "serif",
        ],
        code: [
          "Menlo",
          "Consolas",
          "Liberation Mono",
          "Fira Code",
          "Courier New",
          "monospace",
        ],
      },
      fontSize: {
        h1: ["1.75em", "1.25"],
        h2: ["1.5em", "1.25"],
        h3: ["1.25em", "1.25"],
        h4: ["1.125em", "1.25"],
        h5: ["1em", "1.25"],
        h6: ["0.875em", "1.25"],
        paragraph: ["1em", "1.6"],
        complementary: ["0.875em", "1.6"],
        small: ["0.75em", "1.6"],
      },
    },
    colors: {
      black: colors.black,
      white: colors.white,
      gray: {
        100: "#f8f9fa",
        200: "#eaecf0",
        300: "#c8ccd1",
        400: "#a2a9b1",
        500: "#72777d",
        600: "#54595d",
        650: "#404244",
        700: "#202122",
      },
      blue: {
        100: "#eaf3ff",
        500: "#447ff5",
        600: "#36c",
        700: "#2a4b8d",
      },
      red: {
        100: "#fee7e6",
        500: "#ff4242",
        600: "#d73333",
        700: "#b32424",
      },
      green: {
        100: "#d5fdf4",
        500: "#00af89",
        600: "#14866d",
        700: "#096450",
      },
      yellow: {
        100: "#fef6e7",
        500: "#fc3",
        600: "#edab00",
        700: "#ac6600",
      },
    },
  },
  plugins: [],
};

import express from "express";
import cors from "cors";
import { exec } from "child_process";
import path from "path";
import { createProxyMiddleware } from "http-proxy-middleware";
import { fileURLToPath } from "url";
import { dirname } from "path";
import { WEBHOOK_CREDENTIALS } from "./server/webhook/webhook-credentials.js";
import jsonBuilder from "./server/webhook/json-builder.js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const app = express();
const PORT = process.env.NODE_ENV === "development" ? 5173 : parseInt(process.env.PORT, 10);

app.use(cors());
app.use(express.static("dist"));
app.use("/storage", express.static(path.join(__dirname, "storage")));
app.use("/storage_dev", express.static(path.join(__dirname, "storage_dev")));

/* ------------------------------
   REDIRECTS
------------------------------ */

// Redirect old storage/email path to storage/fr-email-templates/templates-shared
app.use((req, res, next) => {
  if (req.path.startsWith("/storage/email")) {
    const newPath = req.path.replace("/storage/email", "/storage/fr-email-templates/templates-shared");
    return res.redirect(301, newPath);
  }
  next();
});

/* ------------------------------
   ENDPOINTS
------------------------------ */

// GitLab webhook - pulls from [branch] templates-shared on fr-email-templates
app.post("/gitlab-webhook/templates-shared-sync", (req, res) => {
  const gitlabToken = req.headers["x-gitlab-token"];

  // Validate the secret token
  if (gitlabToken !== WEBHOOK_CREDENTIALS.secretToken) {
    console.error("Unauthorized webhook attempt. Invalid token. Endpoint possibly triggered from source that isn't GitLab.");
    return res.status(403).json({ error: "Unauthorized. Invalid token." });
  }

  console.log("Webhook triggered successfully for '/gitlab-webhook/templates-shared-sync'.");

  // Send the 202 Accepted response right after validation
  res.status(202).json({ message: "Webhook received and is being processed." });

  // Path to the submodule
  const submodulePath = process.env.NODE_ENV === "development" ? path.resolve(__dirname, "storage_dev/fr-email-templates/templates-shared") : path.resolve(__dirname, "storage/fr-email-templates/templates-shared");

  // Perform git pull on the submodule asynchronously
  exec(`GIT_SSH_COMMAND="ssh -i ${path.resolve(__dirname, "server/webhook/gitlab-wmf-ssh-key")}" git pull`, { cwd: submodulePath }, (error, stdout, stderr) => {
    if (error) {
      console.error(`Error executing git pull: ${error.message}`);
      return;
    }

    if (stderr) {
      console.error(`Git pull stderr: ${stderr}`);
    }

    console.log(`Git pull output: ${stdout}`);

    // Build .json files in storage/data
    try {
      jsonBuilder.build();
      console.log("JSON files built successfully.");
    } catch (error) {
      console.error("An error occurred during the JSON build process:", error);
      // Log the error, or set up a notification system to notify about the failure
    }
  });
});

/* ------------------------------
   PROXIES
------------------------------ */

// Define the proxy route and target
const proxy = createProxyMiddleware("/email-preview", {
  target: "https://links.email.wikimedia.org",
  changeOrigin: true,
  pathRewrite: {
    "^/email-preview": "",
  },
});

// Apply the proxy to the app
app.use(proxy);

app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";

export default defineConfig({
  plugins: [vue()],
  server: {
    proxy: {
      "/api": "http://localhost:5173",
      "/email-preview": {
        target: "https://links.email.wikimedia.org", // Your target server
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/email-preview/, ""),
      },
    },
  },
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./"),
    },
  },
});
